#ifndef B80_H
#define B80_H
#include <stdint.h>



int doOP(unsigned char op);

typedef uint8_t (*InFunc)(void *, uint8_t);

typedef void (*OutFunc)(void *, uint8_t, uint8_t);
typedef union {
	struct {
		uint8_t l;
		uint8_t h;
	};
	uint16_t w;
} ShortReg;	

typedef struct {
	uint8_t *ram;
	uint16_t pc;
	uint16_t sp;

	uint16_t psw;
	ShortReg bc,de,hl;
	int irq;
	InFunc *in;
	OutFunc *out;
	int type;
	int running;
	uint64_t cycles;
} Cpu80;
enum {
	C_I8080,
	C_Z80,
	C_GB
};
Cpu80 *new_cpu(int type, InFunc in, OutFunc out);
int do_op(Cpu80 *cpu);
#define A (PSW >>8) 
//#define P (PSW & 0xFF)
#endif
