#include "B80.h"
#include <stdio.h>
#define DEBUG
void print_p(Cpu80 *cpu)
{
    printf("P:");
    if (cpu->psw & 0x80)printf("S");
    else printf("s");
    if (cpu->psw & 0x40)printf("Z");
    else printf("z");
  //  if (cpu->psw & 0x20)printf("X");
 //   else printf("0");
    if (cpu->psw & 0x10)printf("A");
    else printf("a");
 //   if (cpu->psw & 0x08)printf("X");
//    else printf("0");
    if (cpu->psw & 0x04)printf("P");
    else printf("p");
//    if (cpu->psw & 0x02)printf("1");
  //  else printf("x");
    if (cpu->psw & 0x01)printf("C");
    else printf("c");
}



static void undef_op(Cpu80 *cpu)
{
	
	printf("Unimplemented op code %.2x @ %x\n", cpu->ram[cpu->pc], cpu->pc);

	cpu->running = 0;		
}
#define RAM_ABS (cpu->ram[cpu->pc+1] | (cpu->ram[cpu->pc+2] << 8))
#define RAM_IMM (cpu->ram[cpu->pc+1]) 

#define LXI(x) \
	x = RAM_ABS;\
	cpu->pc += 3;\
	cpu->cycles +=10;


/* NOP - 0x00 */
static void nop(Cpu80 *cpu)
{
		#ifdef DEBUG 
		printf("NOP\n"); 
		#endif
		cpu->pc++;
		cpu->cycles += 4;

}
/* MOVI B - 0x06 */
static void mvi_b(Cpu80 *cpu)
{
	#ifdef DEBUG
	printf("MVI B,0x%.2x\n",RAM_IMM);
	#endif
		
	cpu->bc.h = RAM_IMM << 8;
	cpu->pc += 2;
	cpu->cycles = 7;

}
/* LXI SP - 0x31 */
static void lxi_sp(Cpu80 *cpu)
{
	#ifdef DEBUG
	fprintf(stderr,"LXI SP, 0x%.4X\n", RAM_ABS);
	#endif
	LXI(cpu->sp)
}
/* JMP - 0xC3 */
static void jmp(Cpu80 *cpu)
{
	#ifdef DEBUG
	printf("JMP 0x%.4X\n", RAM_ABS);
	#endif

	cpu->pc = RAM_ABS; 
	cpu->cycles += 10;
}

void (*opcodes[256])(Cpu80 *) = {
	/*																																				*/
	nop,	 undef_op,	undef_op,undef_op,	undef_op,undef_op,	mvi_b  	,undef_op, undef_op,undef_op,undef_op,undef_op,undef_op,undef_op,undef_op,undef_op,//0
	undef_op,undef_op,	undef_op,undef_op,	undef_op,undef_op,	undef_op,undef_op, undef_op,undef_op,undef_op,undef_op,undef_op,undef_op,undef_op,undef_op,//1
	undef_op,undef_op,	undef_op,undef_op,	undef_op,undef_op,	undef_op,undef_op, undef_op,undef_op,undef_op,undef_op,undef_op,undef_op,undef_op,undef_op,//2
	undef_op,  lxi_sp,	undef_op,undef_op,	undef_op,undef_op,	undef_op,undef_op, undef_op,undef_op,undef_op,undef_op,undef_op,undef_op,undef_op,undef_op,//3
	undef_op,undef_op,	undef_op,undef_op,	undef_op,undef_op,	undef_op,undef_op, undef_op,undef_op,undef_op,undef_op,undef_op,undef_op,undef_op,undef_op,//4
	undef_op,undef_op,	undef_op,undef_op,	undef_op,undef_op,	undef_op,undef_op, undef_op,undef_op,undef_op,undef_op,undef_op,undef_op,undef_op,undef_op,//5
	undef_op,undef_op,	undef_op,undef_op,	undef_op,undef_op,	undef_op,undef_op, undef_op,undef_op,undef_op,undef_op,undef_op,undef_op,undef_op,undef_op,//6
	undef_op,undef_op,	undef_op,undef_op,	undef_op,undef_op,	undef_op,undef_op, undef_op,undef_op,undef_op,undef_op,undef_op,undef_op,undef_op,undef_op,//7
	undef_op,undef_op,	undef_op,undef_op,	undef_op,undef_op,	undef_op,undef_op, undef_op,undef_op,undef_op,undef_op,undef_op,undef_op,undef_op,undef_op,//8
	undef_op,undef_op,	undef_op,undef_op,	undef_op,undef_op,	undef_op,undef_op, undef_op,undef_op,undef_op,undef_op,undef_op,undef_op,undef_op,undef_op,//9
	undef_op,undef_op,	undef_op,undef_op,	undef_op,undef_op,	undef_op,undef_op, undef_op,undef_op,undef_op,undef_op,undef_op,undef_op,undef_op,undef_op,//a
	undef_op,undef_op,	undef_op,undef_op,	undef_op,undef_op,	undef_op,undef_op, undef_op,undef_op,undef_op,undef_op,undef_op,undef_op,undef_op,undef_op,//b
	
	undef_op,undef_op,	undef_op,jmp,		undef_op,undef_op,	undef_op,undef_op,	undef_op,undef_op,	undef_op,undef_op,	undef_op,undef_op,	undef_op,undef_op,//c
	undef_op,undef_op,	undef_op,undef_op,	undef_op,undef_op,	undef_op,undef_op, undef_op,undef_op,undef_op,undef_op,undef_op,undef_op,undef_op,undef_op,//d
	undef_op,undef_op,	undef_op,undef_op,	undef_op,undef_op,	undef_op,undef_op, undef_op,undef_op,undef_op,undef_op,undef_op,undef_op,undef_op,undef_op,//e
	undef_op,undef_op,	undef_op,undef_op,	undef_op,undef_op,	undef_op,undef_op, undef_op,undef_op,undef_op,undef_op,undef_op,undef_op,undef_op,undef_op//f


};
