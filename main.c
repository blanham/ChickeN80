/* holy shit this thing is awful*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "main.h"
#include "video.h"
#include "util.h"
#include "B80.h"

unsigned char RAM[0x4000];
unsigned char buffer[0x800];//what is this for?
int testlines, testlines2;


unsigned short shiftreg = 0xFFFF;
unsigned char shiftoffset = 0;

int runEMU(int cycles);

typedef struct {
	void *cpu;
	Screen *screen;
	uint8_t *ram;
	uint16_t shift_reg;
	uint8_t  shift_offset;
} Machine;

void invaders_out(void *env, uint8_t port, uint8_t value)
{
	switch(port)
	{
		case 0x2:
		//shiftreg = (((shiftreg << value) & 0xFFFF));
		shiftoffset = (value & 0x7);
		break;
		case 0x4:
		shiftreg = (shiftreg >> 8) + (value<<8);
		break;
		default:
		break;
	}
	
}

uint8_t invaders_in(void *env, uint8_t addr)
{
	
	switch(addr)
	{	case 0x1:
		return 0x1;
		case 0x2:
		return 0;
		
		case 0x3:
		return (((shiftreg << shiftoffset) &0xFF00) >> 8); 
		
		
		default:
		return 0;
	}
}

Machine *invaders_new()
{
	Machine *new = malloc(sizeof(Machine));
	
	new->cpu = (void *)new_cpu(C_I8080, &invaders_in, &invaders_out);
	new->screen = screen_init("ChickenN80", 0, 256, 224, 1, 2);  
	
	new->ram = malloc(0x4000);
	
	Cpu80 *cpu = (Cpu80 *)new->cpu;
	
	cpu->ram = new->ram;
	
	return new;
}


void main_loop(Machine *machine)
{
	Cpu80 *cpu = (Cpu80 *)machine->cpu;
	cpu->running = true;
	
	while(cpu->running)
	{
		do_op(cpu);
		
		draw_buffer(machine->screen, machine->screen->vram);
		SDL_Event *event = &machine->screen->event;
		SDL_PollEvent(event);
		if(event->type==SDL_QUIT) exit(0);
	}

}

int main(int argc, char **argv)
{
	Machine *machine = invaders_new();
	memset(machine->screen->vram, 0x55, 256*224);

	
	rom_load("invaders.h", machine->ram, 0x800, 0);
	rom_load("invaders.g", machine->ram, 0x800, 0x800);
	rom_load("invaders.f", machine->ram, 0x800, 0x1000);
	rom_load("invaders.e", machine->ram, 0x800, 0x1800);

	main_loop(machine);
	
	//invaders_destroy(machine);

	return 0;
}


