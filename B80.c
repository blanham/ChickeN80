#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "main.h"
#include "B80.h"
//#define DEBUG
#include "opcodes.h"
#define long_inxdcx

//1979630
int PC;
int interrupt;
unsigned short BC, DE, HL;
unsigned short SP;

void chknegzero( uint8_t chk) {
}
//would probably be better as a macro
inline void check_flags(Cpu80 *cpu, uint8_t chk) {
	//checks for negative #
    if (chk & 0x80) cpu->psw |= 0x80;
    else cpu->psw &= 0xFF7f;

	//checks for 0
    if (chk == 0) cpu->psw |= 0x40;
	else cpu->psw &= 0xffbf;
	
	if((chk &0xF) == 0xF) cpu->psw |=0x10; //ALTERNATE CARRY FLAG?
	else cpu->psw &= 0xFFEF;
	
	//checks parity
	chk ^= (chk >> 1);
	chk ^= (chk >> 2);
	chk ^= (chk >> 4);
	chk ^= (chk >> 8);
	
/*	if (chk & 0x1) cpu->psw |= 0x4;
	else cpu->psw &= 0xFFFB;
*/
	
	if (chk & 0x1) cpu->psw|= 0x4;
	else cpu->psw  &= 0xFFFB;
	

	//overflow
//	if ((sum + 128) & 0x100) P |= 0x40;
  //  else P &= 0xbf;

}



unsigned char bit;
unsigned char save;
unsigned short sum;
unsigned int save16;

int _LXI(unsigned short *reg)
{	
	*reg = (RAM[PC+2] << 8) + RAM[PC+1];
	PC += 3;
	return 10;
}

Cpu80 *new_cpu(int type, InFunc in, OutFunc out)
{
	Cpu80 *new = malloc(sizeof(Cpu80));

	new->type = type;	
	new->in = &in;
	new->out = &out;
	return new;
}

int do_op(Cpu80 *cpu)
{
	uint8_t op = cpu->ram[cpu->pc];

	opcodes[op](cpu);
	#ifdef DEBUG
	print_p(cpu);
	printf("\tPSW %.4X BC %.4X DE %.4X HL %.4X SP %.4X PC %.4X\n",
		cpu->psw,cpu->bc,cpu->de,cpu->hl,cpu->sp,cpu->pc);	
	#endif

 	return 0;
}

int doOP(unsigned char op)
{ 
	int cycles;
	int chk = 0;
	Cpu80 *cpu;
	testlines--;
	testlines2++;
		#ifdef DEBUG
	
	printf("%.4X\t",PC);
	#endif
	switch(op)
	{
		case 0x0:
		#ifdef DEBUG 
		printf("NOP\n"); 
		#endif
		PC++;
		cycles = 4;
		break;
		
		case 0x1:
		#ifdef DEBUG	
		printf("_LXI B, %.4X\n", (RAM[PC+2] << 8) + RAM[PC+1]);
		#endif
		cycles = _LXI(&BC);
		break;		

		case 0x2:
		#ifdef DEBUG
		printf("STAX B\n");
		#endif
		
		RAM[BC] = cpu->psw >>8;
		PC ++;
		cycles = 5;
		break;
		
		case 0x3:
//may need to set HL == to ++BC
//and 0x13
		#ifdef DEBUG
		printf("INX B\n");
		#endif
		
		BC += 1;
		PC++;
		cycles = 5;
		#ifdef long_inxdcx
		cycles++;
		#endif
		break;
	
		case 0x4:
		#ifdef DEBUG
		
		printf("INR B\n");
		#endif
		
		save = BC >> 8;
		save++;
		chknegzero(save);
		if(save == 0)cpu->psw |= 0x40;
		else cpu->psw &= 0xffbf;
		BC = (BC & 0xFF) + ((save & 0xFF) << 8);
		

		PC++;
		cycles = 5;
		break;
		
		case 0x5:
		#ifdef DEBUG
		
		printf("DCR B\n");
		#endif
		
		save = BC >> 8;
		save--;
		chknegzero(save);
		if(save == 0)cpu->psw |= 0x40;
		else cpu->psw &= 0xffbf;
		BC = (BC & 0xFF) + ((save & 0xFF) << 8);
		

		PC++;
		cycles = 5;
		break;
		
		case 0x6:
		#ifdef DEBUG
		
		printf("MVI B,0x%.2x\n",RAM[PC+1]);
		#endif
		
		BC = (BC & 0x00FF) + (RAM[PC+1]<<8);
		PC += 2;
		cycles = 7;
		break;
			case 0x7:
			#ifdef DEBUG

			printf("RLC\n");
			#endif
			save = (cpu->psw >> 8) << 1;
			//bit = save & 0x1;
			//save = save >> 1;
			save = save | (save >>8);
			//cpu->psw |= bit;
			if(save & 0x80) cpu->psw |= 0x1;
			else cpu->psw &= 0xFFFE;

			cpu->psw = (cpu->psw & 0xFF) + (save << 8);
			PC++;
			break;
		case 0x9:
		#ifdef DEBUG
		printf("DAD B\n");
		#endif

		save16 = HL + BC;
		HL = save16;
		if(save16 >> 16) cpu->psw |= 0x1;
		else cpu->psw &= 0xFFFE;
		PC += 1;
		cycles = 10;
		break;
		
		case 0xA:
		#ifdef DEBUG
		printf("LDAX B\n");
		#endif

		cpu->psw = (cpu->psw & 0xFF) + (RAM[BC]<<8);
		//CPreturn 0;
		PC++;
		cycles = 7;
		break;
		case 0xC:
			#ifdef DEBUG

			printf("INR C\n");
			#endif

			save = BC & 0xFF;
			save++;
			chknegzero(save);
			if(save == 0)cpu->psw |= 0x40;
			else cpu->psw &= 0xffbf;
			BC = (BC & 0xFF00) + ((save & 0xFF));


			PC++;
			cycles = 5;
			break;
		case 0xD:
		#ifdef DEBUG
		printf("DCR C\n");
		#endif

		save = BC & 0xFF;
		save--;
		chknegzero(save);
		BC = (BC & 0xFF00) + ((save & 0xFF));
		PC++;
		cycles = 5;
		break;
		
		case 0xE:
		#ifdef DEBUG
		printf("MVI C,0x%.2x\n",RAM[PC+1]);
		#endif
		
		BC = (BC & 0xFF00) + (RAM[PC+1]);
		PC+=2;
		cycles = 7;
		break;
		
		case 0xF:
		#ifdef DEBUG
		
		printf("RRC\n");
		#endif
		save = cpu->psw >>8;
		//bit = save & 0x1;
		//save = save >> 1;
		save = (save << 7) | (save >>1);
		//cpu->psw |= bit;
		if(save & 0x80) cpu->psw |= 0x1;
		else cpu->psw &= 0xFFFE;
		
		cpu->psw = (cpu->psw & 0xFF) + (save << 8);
		PC++;
		break;
		
		
		case 0x11:
		#ifdef DEBUG
		
		printf("_LXI D, %.4X\n", (RAM[PC+2] << 8) + RAM[PC+1]);
		#endif
		
		cycles = _LXI(&DE);
	//	DE = (RAM[PC+2] << 8) + RAM[PC+1];
	//	PC += 3;
	//	cycles = 10;
		break;
		
		case 0x13:
		#ifdef DEBUG
		printf("INX D\n");
		#endif
		
		DE++;
		PC++;
		cycles = 5;
		#ifdef long_inxdcx
		cycles++;
		#endif
		break;
		
		case 0x14:
		#ifdef DEBUG	
		printf("INR D\n");
		#endif

		save = DE >> 8;
		save++;
		chknegzero(save);
		if(save == 0) cpu->psw |= 0x40;
		else cpu->psw &= 0xffbf;
		DE = (DE & 0xFF) + ((save & 0xFF) << 8);


		PC++;
		cycles = 5;
		break;

			case 0x16:
			#ifdef DEBUG

			printf("MVI D,0x%.2x\n",RAM[PC+1]);
			#endif

			DE = (DE & 0x00FF) + (RAM[PC+1]<<8);
			PC += 2;
			cycles = 7;
			break;
		case 0x19:
		#ifdef DEBUG
		
		printf("DAD D\n");
		#endif
		
		save16 = HL + DE;
		HL = save16;
		if(save16 >> 16) cpu->psw |= 0x1;
		else cpu->psw &= 0xFFFE;
		PC += 1;
		cycles = 10;
		break;
		
		case 0x1A:
		#ifdef DEBUG
		
		printf("LDAX D\n");
		#endif
		
		cpu->psw = (cpu->psw & 0xFF) + (RAM[DE]<<8);
		PC++;
		cycles = 7;
		break;
		case 0x1F:
		#ifdef DEBUG
		
		printf("RAR\n");
		#endif
		save = cpu->psw >>8;
		bit = cpu->psw & 0x1;
		//save = save >> 1;
		save = (bit << 7) | (save >>1);
		//cpu->psw |= bit;
		
		if(cpu->psw & 0x100) cpu->psw |= 0x1;
		else cpu->psw &= 0xFFFE;
		
		cpu->psw = (cpu->psw & 0xFF) + (save << 8);
		PC++;
		break;
	/*	case 0x20:
		#ifdef DEBUG
		printf("NOP\n");
		#endif
		PC++;
		cycles = 4;
	//	exit(1);
		break;*/
		
		case 0x21:
		#ifdef DEBUG
		printf("_LXI H, %.4X\n", (RAM[PC+2] << 8) + RAM[PC+1]);
		#endif
		cycles = _LXI(&HL);
	//	HL = (RAM[PC+2] << 8) + RAM[PC+1];
	//	PC += 3;
	//	cycles = 10;
		break;
		
		case 0x22:
		#ifdef DEBUG
		printf("SHLD 0x%.4X\n", (RAM[PC+2] << 8) + RAM[PC+1]);
		#endif
		RAM[(RAM[PC+2] << 8) + RAM[PC+1]+1] = (HL >>8);
		RAM[(RAM[PC+2] << 8) + RAM[PC+1]] = HL&0xFF;
	//	save16 = RAM[(RAM[PC+2] << 8) + RAM[PC+1]]+ ((RAM[(RAM[PC+2] << 8) + RAM[PC+1] + 1]) << 8);
		//RAM[(RAM[PC+2] << 8) + RAM[PC+1]] = cpu->psw >>8;
	//	HL = save16 & 0xFFFF;
		PC += 3;
		cycles = 13;
		break;
		
		case 0x23:
		#ifdef DEBUG
		
		printf("INX H\n");
		#endif
		
		HL++;
		PC++;
		cycles = 5;
		#ifdef long_inxdcx
		cycles++;
		#endif
		break;
		
		case 0x26:
		#ifdef DEBUG
		
		printf("MVI H,%.2x\n",RAM[PC+1]);
		#endif
		
		HL = (HL & 0xFF) + (RAM[PC+1] << 8);
		PC += 2;
		cycles = 7;
		break;
		
		case 0x29:
		#ifdef DEBUG
		
		printf("DAD H\n");
		#endif
		
		save16 = HL + HL;
		HL = save16;
		if(save16 >> 16) cpu->psw |= 0x1;
		else cpu->psw &= 0xFFFE;
		PC += 1;
		cycles = 10;
		break;
		
		case 0x2A:
		#ifdef DEBUG
		printf("LHLD 0x%.4X\n", (RAM[PC+2] << 8) + RAM[PC+1]);
		#endif
		save16 = RAM[(RAM[PC+2] << 8) + RAM[PC+1]]+ ((RAM[(RAM[PC+2] << 8) + RAM[PC+1] + 1]) << 8);
		//RAM[(RAM[PC+2] << 8) + RAM[PC+1]] = cpu->psw >>8;
		HL = save16 & 0xFFFF;
		PC += 3;
		cycles = 13;
		break;
		case 0x2B:
		#ifdef DEBUG
		printf("DCX H\n");
		#endif
		
		HL--;
		PC++;
		cycles = 5;
		#ifdef long_inxdcx
		cycles++;
		#endif
		break;
		case 0x2E:
		#ifdef DEBUG
		
		printf("MVI L,0x%.2x\n",RAM[PC+1]);
		#endif
		
		HL = (HL & 0xFF00) + (RAM[PC+1]);
		PC++;PC++;
		cycles = 7;
		break;
		
		case 0x31:
		#ifdef DEBUG
		
		printf("_LXI SP, 0x%.4X\n", (RAM[PC+2] << 8) + RAM[PC+1]);
		#endif
		
	//	SP = (RAM[PC+2] << 8) + RAM[PC+1];
		//PC += 3;
		//cycles = 10;
		cycles = 	_LXI(&SP);
		break;
		
		case 0x32:
		#ifdef DEBUG
		
		printf("STA 0x%.4X\n", (RAM[PC+2] << 8) + RAM[PC+1]);
		#endif
		
		RAM[(RAM[PC+2] << 8) + RAM[PC+1]] = cpu->psw >>8;
		PC += 3;
		cycles = 13;
		break;
		
		case 0x35:
		#ifdef DEBUG

		printf("DCR M\n");
		#endif

		save = RAM[HL];
		save--;
		chknegzero(save);
		if(save == 0)cpu->psw |= 0x40;
		else cpu->psw &= 0xffbf;
		RAM[HL] = save;


		PC++;
		cycles = 5;
		break;
		
		case 0x36:
		#ifdef DEBUG
		
		printf("MVI M,%.2x\n",RAM[PC+1]);
		#endif
		
		RAM[HL] = RAM[PC+1];
		PC += 2;
		cycles = 10;
		break;
		
		case 0x37:
		#ifdef DEBUG
		
		printf("STC\n");
		#endif
		
		cpu->psw |= 0x1;	
		PC++;
		cycles = 4;
		break;
		
		case 0x39:
		#ifdef DEBUG
		printf("DAD SP\n");
		#endif

		save16 = HL + SP;
		HL = save16;
		if(save16 >> 16) cpu->psw |= 0x1;
		else cpu->psw &= 0xFFFE;
		PC += 1;
		cycles = 10;
		break;	
			
		case 0x3A:
		#ifdef DEBUG
		
		printf("LDA %.4X\n",(RAM[PC+2] << 8) + RAM[PC+1]);
		#endif
		
		cpu->psw = (cpu->psw & 0xFF) + (RAM[( (RAM[PC+2] << 8) + RAM[PC+1])] << 8);
		//printf("\t%.2X\n", cpu->psw>>8);
		PC+=3;
		cycles = 13;
		break;
		
		case 0x3B:
		#ifdef DEBUG
		printf("DCX SP\n");
		#endif

		SP--;
		
		PC++;
		cycles = 5;
		break;


		case 0x3D:
		#ifdef DEBUG
		printf("DCR A\n");
		#endif

		save = cpu->psw >> 8;
		save--;
		chknegzero(save);
		cpu->psw = (cpu->psw & 0xFF) + ((save << 8));
		PC++;
		cycles = 5;
		break;
		
		case 0x3E:
		#ifdef DEBUG
		
		printf("MVI A,0x%.2x\n",RAM[PC+1]);
		#endif
		
		cpu->psw = (cpu->psw & 0xFF) + ((RAM[PC+1]) <<8);
		PC++;PC++;
		cycles = 7;
		break;
		

		
	/*	case 0x40:
		
		PC++;
		break;*/
		
		case 0x46:
		#ifdef DEBUG
		
		printf("MOV B,M\n");
		#endif
	//	save16 = HL;
		BC = (BC & 0xFF) + (RAM[HL] << 8);
		PC += 1;
		cycles = 7;
		break;
		
		
		case 0x47:
		#ifdef DEBUG
		printf("MOV B,A\n");
		#endif
	 	BC = (BC&0xFF) + (cpu->psw & 0xFF00);
		PC++;
		cycles = 5;
		break;
		
		case 0x4F:
		#ifdef DEBUG
		
		printf("MOV C,A\n");
		#endif
		
		BC = (BC & 0xFF00) + (cpu->psw >> 8);
		PC += 1;
		cycles = 5;
		break;
		
		case 0x56:
		#ifdef DEBUG
		
		printf("MOV D,M\n");
		#endif
		//save16 = DE
	//	save16 = RAM[HL];
		DE = (DE & 0xFF) + (RAM[HL] << 8);
		PC += 1;
		cycles = 7;
		break;
		
		case 0x57:
		#ifdef DEBUG
		printf("MOV D,A\n");
		#endif
	 	DE = (DE&0xFF) + (cpu->psw & 0xFF00);
		PC++;
		cycles = 5;
		break;
		
		case 0x5E:
		#ifdef DEBUG
		
		printf("MOV E,M\n");
		#endif
		
		DE = (DE & 0xFF00) + (RAM[HL]);
		PC += 1;
		cycles = 7;
		break;
		
		case 0x5F:
		#ifdef DEBUG
		
		printf("MOV E,A\n");
		#endif
		
		DE = (DE & 0xFF00) + (cpu->psw >> 8);
		PC += 1;
		cycles = 5;
		break;
		
		case 0x66:
		#ifdef DEBUG
		
		printf("MOV H,M\n");
		#endif
		HL = (HL & 0xFF) + (RAM[HL] << 8);
		PC += 1;
		cycles = 7;
		break;
		
		case 0x67:
		#ifdef DEBUG
		
		printf("MOV H,A\n");
		#endif
	//	save16 = HL&0xFF;
		
		HL = (HL&0xFF) + (cpu->psw & 0xFF00);
		PC++;
		cycles = 5;
		break;
		
		
		case 0x6F:
		#ifdef DEBUG
		
		printf("MOV L,A\n");
		#endif
		
		HL = (HL & 0xFF00) + (cpu->psw >> 8);
		PC += 1;
		cycles = 5;
		break;
		
		
		case 0x70:
		#ifdef DEBUG
		printf("MOV M,B\n");
		#endif
		
		RAM[HL] = BC >> 8;
		PC++;
		cycles = 7;
		break;
				
		case 0x77:
		#ifdef DEBUG
		printf("MOV M,A\n");
		#endif
		
		RAM[HL] = cpu->psw >> 8;
		PC++;
		cycles = 7;
		break;
		case 0x78:
		#ifdef DEBUG	
		printf("MOV A,B\n");
		#endif

		cpu->psw = (cpu->psw & 0xFF) + ((BC & 0xFF00));
		PC++;
		cycles = 5;
		#ifdef long_inxdcx
		cycles=8;
		#endif
		break;
		case 0x79:
		#ifdef DEBUG	
		printf("MOV A,C\n");
		#endif

		cpu->psw = (cpu->psw & 0xFF) + ((BC & 0xFF) << 8);
		PC++;
		cycles = 5;
		#ifdef long_inxdcx
		cycles=8;
		#endif
		break;
		
		case 0x7A:
		#ifdef DEBUG	
		printf("MOV A,D\n");
		#endif

		cpu->psw = (cpu->psw & 0xFF) + (DE & 0xFF00);
		PC++;
		cycles = 5;
		#ifdef long_inxdcx
		cycles=8;
		#endif
		break;
		
		case 0x7B:
		#ifdef DEBUG	
		printf("MOV A,E\n");
		#endif

		cpu->psw = (cpu->psw & 0xFF) + ((DE & 0xFF) << 8);
		PC++;
		cycles = 5;
		
		break;
		
		case 0x7c:
		#ifdef DEBUG
		
		printf("MOV A,H\n");
		#endif
		
		cpu->psw = (cpu->psw & 0xFF) + (HL & 0xFF00);
		PC++;
		cycles = 5;

		break;
		
		case 0x7D:
		#ifdef DEBUG
		
		printf("MOV A,L\n");
		#endif
		
		cpu->psw = (cpu->psw & 0xFF) + ((HL & 0xFF) << 8);
		PC++;
		cycles = 5;

		break;
		
		case 0x7E:
		#ifdef DEBUG
		
		printf("MOV A,M\n");
		#endif
	//	save16 = RAM[HL];
		cpu->psw = (cpu->psw & 0xFF) + (RAM[HL]<<8);
		PC += 1;
		cycles = 7;
		break;
			case 0x90:
			//printf(" %.2X\n", RAM[PC+1]);
			#ifdef DEBUG
			printf("SUB B, %.2x\n", RAM[PC+1]);
			#endif
			save16 = (BC) - (RAM[PC+1] << 8);
			BC = save16 & 0xFFFF;
			chknegzero(BC>>8);

			chk = cpu->psw;
				chk ^= (chk >> 1);
				chk ^= (chk >> 2);
				chk ^= (chk >> 4);
				chk ^= (chk >> 8);
				chk ^= (chk >> 16);


			/*	if (chk & 0x1) cpu->psw |= 0x4;
				else cpu->psw &= 0xFFFB;
			*/

				if (chk & 0x1) cpu->psw |= 0x4;
				else cpu->psw &= 0xFFFB;

			if(save16&0xFFFF0000) cpu->psw |= 0x1;
			PC +=2;
			cycles = 7;
			break;
		case 0xA7:
		#ifdef DEBUG
		
		printf("ANA A\n");
		#endif
		save = cpu->psw>>8;
	//	save16 = save & save;
		save &= save;
		cpu->psw = (cpu->psw & 0xFF) + ((save) << 8);
		cpu->psw &= 0xFFEE;
		chknegzero(save);
	//	cpu->psw &= 0xFFEE;
		
	//	if(!(cpu->psw>>8)) cpu->psw |= 0x40;
	//	else 
		cpu->psw &= 0xFFEF;
		PC++;
		cycles = 4;
		break;
		
			case 0xA8:
			#ifdef DEBUG
			printf("XRA B\n");
			
			#endif

			save = cpu->psw>>8;
			save16 = save ^ (BC>>8);
			save = save16 & 0xFF;
			cpu->psw = (cpu->psw & 0xFF) + ((save) << 8);
			cpu->psw &= 0xFFEE;
			chknegzero(save);
		//	cpu->psw &= 0xFFEE;
			if(!(cpu->psw>>8)) cpu->psw |= 0x40;
			else cpu->psw &= 0xFF3F;
			PC++;
			cycles = 4;
			break;
		
		case 0xAF:
		#ifdef DEBUG
		#endif
		
		printf("XRA A\n");
		save = cpu->psw>>8;
		save16 = save ^ save;
		save ^= save;
		cpu->psw = (cpu->psw & 0xFF) + ((save) << 8);
		cpu->psw &= 0xFFEE;
		chknegzero(save);
	//	cpu->psw &= 0xFFEE;
		if(!(cpu->psw>>8)) cpu->psw |= 0x40;
		else cpu->psw &= 0xFF3F;
		PC++;
		cycles = 4;
		break;
		
		case 0xB0:
		#ifdef DEBUG
		printf("ORA B\n");
		#endif
		save = BC >> 8;
		save16 = (cpu->psw>>8) | save;
		//save &= save;
		cpu->psw = (cpu->psw & 0xFF) + ((save16 & 0xFF) << 8);
		cpu->psw &= 0xFFEE;
		chknegzero(save16&0xFF);
	//	cpu->psw &= 0xFFEE;
		PC++;
		cycles = 4;
		break;
				
		case 0xB6:
		#ifdef DEBUG
		printf("ORA M\n");
		#endif
		save = RAM[HL];
		save16 = (cpu->psw>>8) | save;
		//save &= save;
		cpu->psw = (cpu->psw & 0xFF) + ((save16 & 0xFF) << 8);
		cpu->psw &= 0xFFEE;
		chknegzero(save16&0xFF);
	//	cpu->psw &= 0xFFEE;
		PC++;
		cycles = 4;
		break;
		case 0xBF:
		#ifdef DEBUG
		
		printf("CMP B\n");
		#endif
		
		save = (BC & 0xFF00) >>8;
		sum = (signed)(cpu->psw>>8) - (signed)(save);
		chknegzero(sum&0xFF);
		
	    if (sum>0xff) cpu->psw |= 0x01; else cpu->psw &= 0xfffe;

		//cpu->psw |= (((unsigned char)sum )<< 8);
		PC++;
		cycles = 7;
		
		break;
		case 0xC0:
		#ifdef DEBUG
		printf("RNZ\n");
		#endif
		if(!(cpu->psw & 0x40)){
			PC = (RAM[SP] << 8);
			SP++;
			PC |= RAM[SP];
			SP++;
		//HACK!!!!!!!, 
			PC +=2;
		//
		}
		//printf("PC %x\n", PC);
		PC++;
		//	exit(1);
		cycles = 10;
		break;
		
		case 0xC1:
		#ifdef DEBUG
		
		printf("POP B\n");
		#endif
		
		BC = RAM[SP]; 
		SP++;
		BC |= (RAM[SP] << 8);
		SP++;

		PC++;
		cycles = 11;
		break;
		
		case 0xC2:
		#ifdef DEBUG
		
		printf("JNZ $%.4X\n", (RAM[PC+2] << 8) + RAM[PC+1]);
		#endif
		
					if((((RAM[PC+2] << 8) + RAM[PC+1]) == (0xada)) || ((RAM[PC+2] << 8) + RAM[PC+1]) == (0xa9e))
					{
		//				interrupt = 1;
		//				inside =1;
					//	printf("FUFFUF");

						SP--;
						RAM[SP] = PC & 0xFF;
						SP--;
						RAM[SP] = (0xFF00 & PC) >> 8;
						//PC++;
						PC = 8;
						break;
					//		doOP(0xD7);
					}
					
						if((PC == 0x15CE) && (HL = 0x3EBB))
						{
			//				interrupt = 1;
			//				inside =1;
						//	printf("FUFFUF");
							printf("COCK!");
							
							SP--;
							RAM[SP] = PC & 0xFF;
							SP--;
							RAM[SP] = (0xFF00 & PC) >> 8;
							//PC++;
							PC = 16;
							break;
						//		doOP(0xD7);
						}
		if (cpu->psw & 0x40) PC+=3;
		else{ PC = ((RAM[PC+2] << 8) + RAM[PC+1]);
		#ifdef long_inxdcx
		cycles=15;
		#endif
		break;
			}
	//	if(PC == 0x1258) exit(1);

		cycles = 10;

		break;
		
		case 0xC3:
		#ifdef DEBUG
		
		printf("JMP 0x%.4X\n", (RAM[PC+2] << 8) + RAM[PC+1] );
		#endif
		
		PC = (RAM[PC+2] << 8) + RAM[PC+1];
		cycles = 10;
	//	PC++;PC++;PC++;
		break;
		case 0xC4:
		#ifdef DEBUG
		
		printf("CNZ 0x%.4X\n",  (RAM[PC+2] << 8) + RAM[PC+1]);
		#endif
		
		//STACK OP -- We push PC onto stack
//FIXME: not sure if byte order correct
		if(!(cpu->psw & 0x40)){SP--;

		RAM[SP] = PC & 0xFF;
		SP--;
		RAM[SP] = (0xFF00 & PC) >> 8;
	//	printf("PC%.4X\n",PC);
		PC = (RAM[PC+2] << 8) + RAM[PC+1];
		
	//	exit(0);
		cycles = 17;}
		else{
			cycles = 11;
			PC +=3;
		}
		break;
		case 0xC5:
		#ifdef DEBUG
		printf("PUSH B\n");
		#endif
		SP--;
		RAM[SP] = (0xFF00 & BC) >> 8;
		SP--;
		RAM[SP] = BC & 0xFF;
		PC++;
		cycles = 11;
		break;
		
		case 0xC6:
		//printf(" %.2X\n", RAM[PC+1]);
		save16 = (cpu->psw) + (RAM[PC+1] << 8);
		cpu->psw = save16 & 0xFFFF;
		chknegzero(cpu->psw>>8);
		
		chk = cpu->psw;
			chk ^= (chk >> 1);
			chk ^= (chk >> 2);
			chk ^= (chk >> 4);
			chk ^= (chk >> 8);
			chk ^= (chk >> 16);
			

		/*	if (chk & 0x1) cpu->psw |= 0x4;
			else cpu->psw &= 0xFFFB;
		*/

			if (chk & 0x1) cpu->psw |= 0x4;
			else cpu->psw &= 0xFFFB;
		
		if(save16&0xFFFF0000) cpu->psw |= 0x1;
		PC +=2;
		cycles = 7;
		break;
		
		case 0xC8:
		#ifdef DEBUG
		printf("RZ\n");
		#endif
		if(cpu->psw & 0x40){
		PC = (RAM[SP] << 8);
		SP++;
		PC |= RAM[SP];
		SP++;
//HACK!!!!!!!, 
		PC +=2;
//
		}
		//printf("PC %x\n", PC);
		PC ++;
		//	exit(1);
		cycles = 10;
		break;
	
		case 0xC9:
		#ifdef DEBUG
		
		printf("RET\n");
		#endif
		
		PC = (RAM[SP] << 8);
		SP++;
		PC |= RAM[SP];
		SP++;
		
		//printf("PC %x\n", PC);
		PC +=3;
	//	exit(1);
		cycles = 10;
		break;
		
		case 0xCA:
		#ifdef DEBUG
		printf("JZ $%.4X\n", (RAM[PC+2] << 8) + RAM[PC+1]);
		#endif

		if (cpu->psw & 0x40){ PC = ((RAM[PC+2] << 8) + RAM[PC+1]);
		#ifdef long_inxdcx
		cycles=15;
		#endif
		break;
		}else PC+=3;
	
		//	if(PC == 0x1258) exit(1);

		cycles = 10;

		break;
		
		case 0xCD:
		#ifdef DEBUG
		
		printf("CALL 0x%.4X\n",  (RAM[PC+2] << 8) + RAM[PC+1]);
		#endif
		
		//STACK OP -- We push PC onto stack
//FIXME: not sure if byte order correct
		SP--;

		RAM[SP] = PC & 0xFF;
		SP--;
		RAM[SP] = (0xFF00 & PC) >> 8;
	//	printf("PC%.4X\n",PC);
		PC = (RAM[PC+2] << 8) + RAM[PC+1];
		
	//	exit(0);
		cycles = 17;
		break;
		
		case 0xCF:
		#ifdef DEBUG
		printf("RST 1\n");
		#endif
		SP--;
		RAM[SP] = PC & 0xFF;
		SP--;
		RAM[SP] = (0xFF00 & PC) >> 8;
		PC = 0x10;
		
	//	PC++;
		cycles = 11;
		break;
		
		case 0xD0:
		#ifdef DEBUG
		printf("RNC\n");
		#endif
		if(!(cpu->psw & 0x1)){
			PC = (RAM[SP] << 8);
			SP++;
			PC |= RAM[SP];
			SP++;
	//HACK!!!!!!!, 
			PC +=2;
	//
		}
		//printf("PC %x\n", PC);
		PC ++;
		if(PC == 0x272) PC -=3;
		//	exit(1);
		cycles = 10;
		break;
		
		case 0xD1:
		#ifdef DEBUG
		
		printf("POP D\n");
		#endif
		
		DE = RAM[SP]; 
		SP++;
		DE |= RAM[SP] << 8;
		SP++;

		PC++;
		cycles = 11;
		break;
		
		case 0xD2:
		#ifdef DEBUG
		printf("JNC $%.4X\n", (RAM[PC+2] << 8) + RAM[PC+1]);
		#endif

		if (cpu->psw & 0x1) PC+=3;
		else{ PC = ((RAM[PC+2] << 8) + RAM[PC+1]);
		#ifdef long_inxdcx
		cycles=15;
		#endif
		break;
		}
		//	if(PC == 0x1258) exit(1);

		cycles = 10;

		break;
		
		//case 0xD3:
		//#ifdef DEBUG
		//printf("OUT %.2X\n", RAM[PC+1]);
		//#endif
		//OUT(RAM[PC+1], cpu->psw>>8);
		//PC += 2;
		//cycles = 11;
		//break;
		
		case 0xD5:
		#ifdef DEBUG
		
		printf("PUSH D\n");
		#endif
		
		SP--;
		RAM[SP] = (0xFF00 & DE) >> 8;
		SP--;
		RAM[SP] = DE & 0xFF;
		PC++;
		cycles = 11;
		break;
		
		case 0xD6:
		//printf(" %.2X\n", RAM[PC+1]);
		#ifdef DEBUG
		printf("SUB A, %.2x\n", RAM[PC+1]);
		#endif
		save16 = (cpu->psw) - (RAM[PC+1] << 8);
		cpu->psw = save16 & 0xFFFF;
		chknegzero(cpu->psw>>8);
		
		chk = cpu->psw;
			chk ^= (chk >> 1);
			chk ^= (chk >> 2);
			chk ^= (chk >> 4);
			chk ^= (chk >> 8);
			chk ^= (chk >> 16);
			

		/*	if (chk & 0x1) cpu->psw |= 0x4;
			else cpu->psw &= 0xFFFB;
		*/

			if (chk & 0x1) cpu->psw |= 0x4;
			else cpu->psw &= 0xFFFB;
		
		if(save16&0xFFFF0000) cpu->psw |= 0x1;
		PC +=2;
		cycles = 7;
		break;
		case 0xD7:
		#ifdef DEBUG
		printf("RST 1\n");
		#endif
		SP--;
		RAM[SP] = PC & 0xFF;
		SP--;
		RAM[SP] = (0xFF00 & PC) >> 8;
		PC = 8;

		//	PC++;
		cycles = 11;
		break;
		
		case 0xD8:
		#ifdef DEBUG
		printf("RZ\n");
		#endif
		if(cpu->psw & 0x1){
			PC = (RAM[SP] << 8);
			SP++;
			PC |= RAM[SP];
			SP++;
	//HACK!!!!!!!, 
			PC +=2;
	//
		}
		//printf("PC %x\n", PC);
		PC ++;
		//	exit(1);
		cycles = 10;
		break;

		case 0xDA:
		#ifdef DEBUG
		printf("JC $%.4X\n", (RAM[PC+2] << 8) + RAM[PC+1]);
		#endif

		if (cpu->psw & 0x1){ PC = ((RAM[PC+2] << 8) + RAM[PC+1]);
		#ifdef long_inxdcx
		cycles=15;
		#endif
		break;
		}else PC+=3;
	

		cycles = 10;

		break;
		
		//case 0xDB:
		//#ifdef DEBUG
		//printf("IN %.2X\n", RAM[PC+1]);
		//#endif
		//cpu->psw = (IN(RAM[PC+1]) << 8) + (cpu->psw &0xFF);
		//PC += 2;
		//cycles = 11;
		//break;
		
		case 0xE1:
		#ifdef DEBUG
		
		printf("POP H\n");
		#endif
		
		HL = RAM[SP]; 
		SP++;
		HL |= RAM[SP] << 8;
		SP++;

		PC++;
		cycles = 11;
		break;
		

		case 0xE3:
		#ifdef DEBUG
		printf("XTHL\n");
		#endif
		save16 = RAM[SP]; 
		SP++;
		save16 |= (RAM[SP] << 8);
		SP++;
		
		SP--;
		RAM[SP] = HL & 0xFF;
		
		SP--;
		RAM[SP] = (0xFF00 & HL) >> 8;
		
		
		
		HL = save16;
		PC++;
		cycles = 18;
		break;
		case 0xE5:
		#ifdef DEBUG
		
		printf("PUSH H\n");
		#endif
		
		SP--;
		RAM[SP] = (0xFF00 & HL) >> 8;
		SP--;
		RAM[SP] = HL & 0xFF;
		PC++;
		cycles = 11;
		break;
	
		case 0xE6:
		#ifdef DEBUG
		
		printf("ANI %.2X\n",RAM[PC+1]);
		#endif
		save = cpu->psw>>8;
		save &= RAM[PC+1];
		cpu->psw = (cpu->psw & 0xFF) + (save << 8);
		cpu->psw &= 0xFFEE;
		chknegzero(save);
	//	cpu->psw &= 0xFFEE;
		if(!(cpu->psw>>8)) cpu->psw |= 0x40;
		PC+=2;
		cycles = 7;
		break;
		
		case 0xE9:
		#ifdef DEBUG

		printf("PCHL\n");
		#endif
		PC = HL;
		cycles = 11;
		break;
		case 0xEB:
		#ifdef DEBUG
		
		printf("XCHG\n");
		#endif
		
		save16 = DE;
		DE = HL;
		HL = save16;
		PC++;
		cycles = 5;
		break;
		
		case 0xF1:
		#ifdef DEBUG
		
		printf("POP cpu->psw\n");
		#endif
		
		cpu->psw = RAM[SP]; 
		SP++;
		cpu->psw |= RAM[SP] << 8;
		SP++;

		PC++;
		cycles = 11;
		break;
		
		case 0xF5:
		#ifdef DEBUG
		
		printf("PUSH H\n");
		#endif
		
		SP--;
		RAM[SP] = (0xFF00 & cpu->psw) >> 8;
		SP--;
		RAM[SP] = cpu->psw & 0xFF;
		PC++;
		cycles = 11;
		break;
		case 0xF6:
		#ifdef DEBUG
		
		printf("ORI %.2X\n",RAM[PC+1]);
		#endif
		save = cpu->psw>>8;
		save |= RAM[PC+1];
		cpu->psw = (cpu->psw & 0xFF) + (save << 8);
		cpu->psw &= 0xFFEE;
		chknegzero(save);
	//	cpu->psw &= 0xFFEE;
		if(!(cpu->psw>>8)) cpu->psw |= 0x40;
		PC+=2;
		cycles = 7;
		break;
		case 0xFB:
		#ifdef DEBUG
		printf("EI\n");
		#endif
		printf("INT ON\n");
		interrupt = 1;
		PC++;
		cycles = 4;
		break;
		
		case 0xFE:
		#ifdef DEBUG
		
		printf("CPI%.2X\n",RAM[PC+1]);
		#endif
		
		save = (cpu->psw & 0xFF00) >>8;
		sum = (signed)save - (signed)RAM[PC+1];
		chknegzero(sum&0xFF);
		
	    if (sum>0xff) cpu->psw |= 0x01; else cpu->psw &= 0xfffe;

		//cpu->psw |= (((unsigned char)sum )<< 8);
		PC +=2;
		cycles = 7;
		
		break;
		
		default:
		//printp();
		
		printf("\tcpu->psw %.4X BC %.4X DE %.4X HL %.4X SP %.4X PC %.4X\t",cpu->psw,BC,DE,HL,SP,PC);
		
		printf("Unimplemented op code %.2x @ operation %i\n", op, testlines2);
		return 0;//exit(1);
	}
		#ifdef DEBUG
   
	//printp();
	//	#ifdef DEBUG
	
	printf("\tcpu->psw %.4X BC %.4X DE %.4X HL %.4X SP %.4X PC %.4X ",cpu->psw,BC,DE,HL,SP,PC);
	printf("STK %.2x %.2x", RAM[SP], RAM[SP+1]);
	printf(" @ op %i\n",  testlines2);
	
		#endif
	
	if((RAM[SP-1] == 0x6F) && (RAM[SP] == 0x05)){
		//printp();
		
		printf("\tcpu->psw %.4X BC %.4X DE %.4X HL %.4X SP %.4X\n",cpu->psw,BC,DE,HL,SP);
		
		printf("PLAYER %.2x @ operation %i\n", op, testlines2);
		exit(1);
	}
	/*
	cpu->psw = RAM[SP]; 
	SP++;
	cpu->psw |= RAM[SP] << 8;
	SP++;
	
	*/
	if((RAM[0x2F34] != 0) &&(RAM[0x2F34] != 0x7F)){
		//printp();
		
		printf("\tcpu->psw %.4X BC %.4X DE %.4X HL %.4X SP %.4X\n",cpu->psw,BC,DE,HL,SP);
		
		printf("PLAYER %.2x @ operation %i\n", op, testlines2);
	//	return 0;
	}
	
	
	
	return cycles;
} 
