CC=gcc
CFLAGS=-c -Wall -std=gnu99 -arch i386 -Ilib/libchicken/
SDLFLAGS=`sdl-config --cflags --libs` -arch i386 -framework OpenGL -framework Cocoa 

SOURCES=main.c B80.c 
OBJECTS=$(SOURCES:.c=.o) lib/libchicken/libchicken.a
EXECUTABLE=B80

all: $(SOURCES) $(EXECUTABLE)

$(EXECUTABLE): $(OBJECTS)
	$(CC) $(OBJECTS) $(SDLFLAGS) -o $@

.c.o:
	$(CC) $(CFLAGS) $< -o $@

depend: .depend

.depend: $(SOURCES)
	rm -f ./.depend
	$(CC) $(CFLAGS) -MM $^ >> ./.depend;

include .depend

clean:
	rm -rf *.o $(EXECUTABLE)
